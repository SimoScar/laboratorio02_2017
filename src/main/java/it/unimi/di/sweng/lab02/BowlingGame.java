package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {

	private static final int MAX_ROLLS = 20;
	private int[] rolls = new int[21];
	private int currentRoll = 0;
	private int strikeNumber = 0;
	private int strikes = 0;
	
	@Override
	public void roll(int pins) {
		rolls[currentRoll++] = pins;
		if (pins == 10){
			if ((currentRoll+strikes++) == MAX_ROLLS - 1)
				strikeNumber -= 1;
		}
	}

	@Override
	public int score() {
		int score = 0;
		for (int currentRoll = 0; currentRoll < MAX_ROLLS-strikeNumber; currentRoll++){
			if (isStrike(currentRoll)){
				strikeNumber++;
				score += rolls[currentRoll+1] + rolls[currentRoll+2];
				}
			else if (isSpare(currentRoll))
				score += rolls[currentRoll+2];
			score += rolls[currentRoll];
		}
		return score;
	}

	private boolean isSpare(int currentRoll){
		return currentRoll<MAX_ROLLS - strikeNumber -4 && rolls[currentRoll] + rolls[currentRoll+1] ==10 && isEven(currentRoll);
	}
	
	private boolean isStrike(int currentRoll){
		return currentRoll<MAX_ROLLS - strikeNumber - 3&& rolls[currentRoll]== 10 && isEven(currentRoll);
	}
	
	private boolean isEven(int currentRoll){  
		return (currentRoll + strikeNumber) % 2 == 0; 
	}
}